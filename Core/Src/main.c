/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <stdbool.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define NUMBER_OF_EATERS 5
#define NOTIFY_BIT 0x01

#define NOTIFY_THREAD1 NOTIFY_BIT
#define NOTIFY_THREAD2 NOTIFY_BIT
#define NOTIFY_THREAD3 NOTIFY_BIT
#define NOTIFY_THREAD4 NOTIFY_BIT
#define NOTIFY_THREAD5 NOTIFY_BIT
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart2;

osThreadId defaultTaskHandle;
osThreadId myTask01Handle;
osThreadId myTask02Handle;
osThreadId myTask03Handle;
osThreadId myTask04Handle;
osThreadId myTask05Handle;
osMutexId soupMutexHandle;
osMutexId uartMutexHandle;
/* USER CODE BEGIN PV */
static int soupServings = 1000;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
void StartDefaultTask(void const * argument);
void StartTask01(void const * argument);
void StartTask02(void const * argument);
void StartTask03(void const * argument);
void StartTask04(void const * argument);
void StartTask05(void const * argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Create the mutex(es) */
  /* definition and creation of soupMutex */
  osMutexDef(soupMutex);
  soupMutexHandle = osMutexCreate(osMutex(soupMutex));

  /* definition and creation of uartMutex */
  osMutexDef(uartMutex);
  uartMutexHandle = osMutexCreate(osMutex(uartMutex));

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of myTask01 */
  osThreadDef(myTask01, StartTask01, osPriorityNormal, 0, 64);
  myTask01Handle = osThreadCreate(osThread(myTask01), NULL);

  /* definition and creation of myTask02 */
  osThreadDef(myTask02, StartTask02, osPriorityNormal, 0, 64);
  myTask02Handle = osThreadCreate(osThread(myTask02), NULL);

  /* definition and creation of myTask03 */
  osThreadDef(myTask03, StartTask03, osPriorityNormal, 0, 64);
  myTask03Handle = osThreadCreate(osThread(myTask03), NULL);

  /* definition and creation of myTask04 */
  osThreadDef(myTask04, StartTask04, osPriorityNormal, 0, 64);
  myTask04Handle = osThreadCreate(osThread(myTask04), NULL);

  /* definition and creation of myTask05 */
  osThreadDef(myTask05, StartTask05, osPriorityNormal, 0, 64);
  myTask05Handle = osThreadCreate(osThread(myTask05), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_9;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 5;
  RCC_OscInitStruct.PLL.PLLN = 71;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV6;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV4;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the main internal regulator output voltage
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, RED_LED_Pin|BLUE_LED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GREEN_LED_GPIO_Port, GREEN_LED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : INPUT_Pin */
  GPIO_InitStruct.Pin = INPUT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(INPUT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : RED_LED_Pin BLUE_LED_Pin */
  GPIO_InitStruct.Pin = RED_LED_Pin|BLUE_LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : GREEN_LED_Pin */
  GPIO_InitStruct.Pin = GREEN_LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GREEN_LED_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
int hungry_person(int id, uint32_t id_notify_bit)
{
	uint32_t soup_served = 0;
	uint32_t notifValue;

	while(soupServings >= 0)
	{
		while((id != (soupServings % NUMBER_OF_EATERS)) && (soupServings >= 0))
		{
			//wait in the queue, we only clear the bit on the notify upon exit
			//and we wait indefinitely until notified.
			xTaskNotifyWait(pdFALSE, id_notify_bit, &notifValue, portMAX_DELAY);
			if ((notifValue & id_notify_bit) != 0x00)	//Check if notify value is set
			{
				if (soupServings >= 0)
				{
					xSemaphoreTake(soupMutexHandle, portMAX_DELAY); //Lock the Mutex
					soupServings--;     //Decrement the resource being soup
					soup_served++;      //Increment the soup counter.
					xSemaphoreGive(soupMutexHandle); //Unlock the mutex

					HAL_Delay(5);   //keeps a race conditioning from occurring.

					//notify all
					if ((id != soupServings % NUMBER_OF_EATERS) && (soupServings >= 0))
					{
						xTaskNotify(myTask01Handle, NOTIFY_THREAD1, eSetBits);
					}
					if ((id != soupServings % NUMBER_OF_EATERS) && (soupServings >= 0))
					{
						xTaskNotify(myTask02Handle, NOTIFY_THREAD2, eSetBits);
					}
					if ((id != soupServings % NUMBER_OF_EATERS) && (soupServings >= 0))
					{
                        xTaskNotify(myTask03Handle, NOTIFY_THREAD3, eSetBits);
					}
                    if ((id != soupServings % NUMBER_OF_EATERS) && (soupServings >= 0))
                    {
                        xTaskNotify(myTask04Handle, NOTIFY_THREAD4, eSetBits);
                    }
                    if ((id != soupServings % NUMBER_OF_EATERS) && (soupServings >= 0))
                    {
                        xTaskNotify(myTask05Handle, NOTIFY_THREAD5, eSetBits);
                    }
				}
				break;
			}
		}
	}
	// print number of lid put back # of times.
	return soup_served;
}

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN 5 */
  /* Infinite loop */
  for(;;)
  {
	if (HAL_GPIO_ReadPin(INPUT_GPIO_Port, INPUT_Pin) == GPIO_PIN_SET)
	{
		xTaskNotify(myTask01Handle, NOTIFY_THREAD1, eSetBits);
		xTaskNotify(myTask02Handle, NOTIFY_THREAD2, eSetBits);
		xTaskNotify(myTask03Handle, NOTIFY_THREAD3, eSetBits);
        xTaskNotify(myTask03Handle, NOTIFY_THREAD4, eSetBits);
        xTaskNotify(myTask03Handle, NOTIFY_THREAD5, eSetBits);

		HAL_GPIO_WritePin(RED_LED_GPIO_Port, RED_LED_Pin, GPIO_PIN_SET);
		xSemaphoreTake(uartMutexHandle, portMAX_DELAY);
		HAL_UART_Transmit(&huart2, (uint8_t*)"Button Pressed\r\n", 17, 5);
		xSemaphoreGive(uartMutexHandle);
	}
    osDelay(200);
    HAL_GPIO_WritePin(RED_LED_GPIO_Port, RED_LED_Pin, GPIO_PIN_RESET);
  }
  /* USER CODE END 5 */
}

/* USER CODE BEGIN Header_StartTask01 */
/**
* @brief Function implementing the myTask01 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask01 */
void StartTask01(void const * argument)
{
  /* USER CODE BEGIN StartTask01 */
  /* Infinite loop */
  char snum[5];
  int soup_served1 = 0;
  bool output_to_uart = false;
  /* Infinite loop */
  for(;;)
  {
	soup_served1 += hungry_person(0, NOTIFY_THREAD1);

	if ((soupServings < 0) && (output_to_uart == false))
	{
		itoa(soup_served1, snum, 10);
		xSemaphoreTake(uartMutexHandle, portMAX_DELAY);			//take the mutex
		HAL_UART_Transmit(&huart2, (uint8_t*)"Thread1 ate ", sizeof("Thread1 ate "), 5);
		HAL_UART_Transmit(&huart2, (uint8_t*)snum, sizeof(snum), 5);
		HAL_UART_Transmit(&huart2, (uint8_t*)" soups \r\n", sizeof(" soups \r\n"), 5);
		output_to_uart = true;
		xSemaphoreGive(uartMutexHandle);						//give the mutex
	}
	osDelay(200);
  }
  /* USER CODE END StartTask01 */
}

/* USER CODE BEGIN Header_StartTask02 */
/**
* @brief Function implementing the myTask02 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask02 */
void StartTask02(void const * argument)
{
  /* USER CODE BEGIN StartTask02 */
  /* Infinite loop */
  char snum[5];
  int soup_served2 = 0;
  bool output_to_uart = false;
  /* Infinite loop */
  for(;;)
  {
	soup_served2 += hungry_person(1, NOTIFY_THREAD2);

	if ((soupServings < 0) && (output_to_uart == false))
	{
		itoa(soup_served2, snum, 10);
		xSemaphoreTake(uartMutexHandle, portMAX_DELAY);			//take the mutex
		HAL_UART_Transmit(&huart2, (uint8_t*)"Thread2 ate ", sizeof("Thread2 ate "), 5);
		HAL_UART_Transmit(&huart2, (uint8_t*)snum, sizeof(snum), 5);
		HAL_UART_Transmit(&huart2, (uint8_t*)" soups\r\n", sizeof(" soups\r\n"), 5);
		output_to_uart = true;
		xSemaphoreGive(uartMutexHandle);						//give the mutex
	}
    osDelay(200);
  }
  /* USER CODE END StartTask02 */
}

/* USER CODE BEGIN Header_StartTask03 */
/**
* @brief Function implementing the myTask03 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask03 */
void StartTask03(void const * argument)
{
  /* USER CODE BEGIN StartTask03 */
  /* Infinite loop */
    char snum[5];
    int soup_served3 = 0;
    bool output_to_uart = false;
    /* Infinite loop */
    for(;;)
    {
      soup_served3 += hungry_person(2, NOTIFY_THREAD3);

      if ((soupServings < 0) && (output_to_uart == false))
      {
  		  itoa(soup_served3, snum, 10);
  		  xSemaphoreTake(uartMutexHandle, portMAX_DELAY);			//take the mutex
		  HAL_UART_Transmit(&huart2, (uint8_t*)"Thread3 ate ", sizeof("Thread3 ate "), 5);
		  HAL_UART_Transmit(&huart2, (uint8_t*)snum, sizeof(snum), 5);
		  HAL_UART_Transmit(&huart2, (uint8_t*)" soups\r\n", sizeof(" soups\r\n"), 5);
          output_to_uart = true;
          xSemaphoreGive(uartMutexHandle);                        //give the mutex
      }
      osDelay(200);
    }
  /* USER CODE END StartTask03 */
}

/* USER CODE BEGIN Header_StartTask04 */
/**
* @brief Function implementing the myTask04 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask04 */
void StartTask04(void const * argument)
{
  /* USER CODE BEGIN StartTask04 */
  /* Infinite loop */
    char snum[5];
    int soup_served4 = 0;
    bool output_to_uart = false;
    /* Infinite loop */
    for(;;)
    {
      soup_served4 += hungry_person(3, NOTIFY_THREAD3);

      if ((soupServings < 0) && (output_to_uart == false))
      {
    	  itoa(soup_served4, snum, 10);
    	  xSemaphoreTake(uartMutexHandle, portMAX_DELAY);			//take the mutex
  		  HAL_UART_Transmit(&huart2, (uint8_t*)"Thread4 ate ", sizeof("Thread4 ate "), 5);
  		  HAL_UART_Transmit(&huart2, (uint8_t*)snum, sizeof(snum), 5);
  		  HAL_UART_Transmit(&huart2, (uint8_t*)" soups\r\n", sizeof(" soups\r\n"), 5);
          output_to_uart = true;
          xSemaphoreGive(uartMutexHandle);                        //give the mutex
      }
      osDelay(200);
    }
  /* USER CODE END StartTask04 */
}

/* USER CODE BEGIN Header_StartTask05 */
/**
* @brief Function implementing the myTask05 thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTask05 */
void StartTask05(void const * argument)
{
  /* USER CODE BEGIN StartTask05 */
  /* Infinite loop */
    char snum[5];
    int soup_served5 = 0;
    bool output_to_uart = false;
    /* Infinite loop */
    for(;;)
    {
      soup_served5 += hungry_person(4, NOTIFY_THREAD1);

      if ((soupServings < 0) && (output_to_uart == false))
      {
    	  itoa(soup_served5, snum, 10);
    	  xSemaphoreTake(uartMutexHandle, portMAX_DELAY);			//take the mutex
  		  HAL_UART_Transmit(&huart2, (uint8_t*)"Thread5 ate ", sizeof("Thread5 ate "), 5);
  		  HAL_UART_Transmit(&huart2, (uint8_t*)snum, sizeof(snum), 5);
  		  HAL_UART_Transmit(&huart2, (uint8_t*)" soups\r\n", sizeof(" soups\r\n"), 5);
          output_to_uart = true;
          xSemaphoreGive(uartMutexHandle);                        //give the mutex
      }
      osDelay(200);
    }
  /* USER CODE END StartTask05 */
}

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM7 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM7) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
